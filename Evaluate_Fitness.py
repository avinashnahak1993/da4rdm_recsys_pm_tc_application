import pandas as pd
import pm4py


def calculate_fitness(phase_data, log):
    phase_to_mine = pm4py.format_dataframe(phase_data, case_id='SessionId',
                                           activity_key='Operation', timestamp_key='Timestamp')
    phase_log = pm4py.convert_to_event_log(phase_to_mine)
    net, initial_marking, final_marking = pm4py.discover_petri_net_inductive(phase_log)
    alignment_based_fitness = pm4py.fitness_alignments(log, net, initial_marking, final_marking)
    return alignment_based_fitness['averageFitness']


def evaluate_fitness(log_to_confirm):
    phase_data_planning = pd.read_csv("Data/Inputs/Phase_Planning.csv")
    phase_data_production = pd.read_csv("Data/Inputs/Phase_Production.csv")
    phase_data_analysis = pd.read_csv("Data/Inputs/Phase_Analysis.csv")
    phase_data_archival = pd.read_csv("Data/Inputs/Phase_Archival.csv")
    phase_data_access = pd.read_csv("Data/Inputs/Phase_Access.csv")
    phase_data_reuse = pd.read_csv("Data/Inputs/Phase_Reuse.csv")
    fitness_list = list()
    phase_list = [phase_data_planning, phase_data_analysis, phase_data_archival, phase_data_production,
                  phase_data_access, phase_data_reuse]
    for phase in phase_list:
        fitness = calculate_fitness(phase, log_to_confirm)
        fitness_list.append(fitness)
    return fitness_list
